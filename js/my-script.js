let page = (function (){
    this.popularityFn = function(sliderId, selectorId) {
        let slider = document.getElementById(sliderId);
        let selector = $('#'+ selectorId);
        selector.css({"font-size": slider.value + "px"}); // default
        slider.oninput = function() {
            selector.css({"font-size": this.value + "px"}); // adjust
        }
    }

    this.processMultiPopularityFn = function() {
        for (let i=0; i<employees.length; i++) {
            popularityFn('slider-' + employees[i].id, 'output-slider-' + employees[i].id)
        }
    }

    this.initNavPillTemplate = function() {
        let rawHTML = $('#nav-pill-template').html();
        let navPillTemplate = Handlebars.compile(rawHTML);
        let html = '';
        for (let i=0; i<employees.length; i++) {
            html += navPillTemplate(employees[i]);
        }
        $('#js-nav-pill').html(html);
    }

    this.initProfileDetailTemplate = function() {
        let rawHTML = $('#profile-detail-template').html();
        let profileDetailTemplate = Handlebars.compile(rawHTML);
        let html = '';
        for (let i=0; i<employees.length; i++) {
            html += profileDetailTemplate(employees[i]);
        }
        $('#js-profile-detail').html(html);
    }

    this.initPage = function() {
        this.initNavPillTemplate();
        this.initProfileDetailTemplate();
        processMultiPopularityFn();
        console.log("For me, there are still a lot of things to do with the page for the better UI as well as e scaling. Thank you for this interesting challenge! :)")
    }
    return this;
})();

$(document).ready(function() {
    page.initPage();
});